/**
 * KindEditorExtend 相关扩展插件
 * 使用方法如下：其中初始化编译器的时候，要将相对应的参数传递过去，之后就可以使用相关的方法进行处理操作了
 * 	var editor = KindEditorExtend.initEditor({
		contextURL : context_,
		contextPath : "admin",
		detailPath : sysmanUserId,
		kindEditorId : "content1",
	});
	console.log(editor.html("sdafasdf"));
 */
var KindEditorExtend = function(){
	
	var defaults = {
		contextURL : "contextURL",
		contextPath : "contextPath",
		detailPath : "anonymous",
		kindEditorId : "content",
	};
	
	return {
	
		initEditor : function(options) {
			
			defaults = $.extend(defaults, options);
			
			KindEditor.ready(function(K) {
				K.create("textarea[name="+defaults.kindEditorId+"]", {
					cssPath : ""+defaults.contextURL+"/kindeditor/plugins/code/prettify.css",
					uploadJson : ""+defaults.contextURL+"/fileUpload?contextPath="+defaults.contextPath+"&detailPath="+defaults.detailPath,
					fileManagerJson : ""+defaults.contextURL+"/fileManager?contextPath="+defaults.contextPath+"&detailPath="+defaults.detailPath,
					allowFileManager : true,
					afterCreate : function() {
						
					}
				});
				prettyPrint();
			});
			
			return $("textarea[name="+defaults.kindEditorId+"]");
			
		},
		
		anotherMethod : function() {
			console.log("This is another method, extend this javascript file");
		}
	
	};
}();