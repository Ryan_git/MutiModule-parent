<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String context = request.getContextPath();
	pageContext.setAttribute("context_", context);
%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<!-- 引入相关的js文件，相对路径  -->
<link
	href="${pageContext.request.contextPath}/css/uploadify/uploadify.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery/jquery-v1.11.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/uploadify/jquery.uploadify.js"></script>

<!-- 执行上传文件操作的函数 -->
<script type="text/javascript">
	var context_ = '${context_}';

	$(function() {
		$("#uploadify").uploadify({
			//指定swf文件
			'swf' : context_ + '/js/uploadify/uploadify.swf',
			//后台处理的页面
			'uploader' : 'fileUpload?contextPath=demo&detailPath=anonymous', //需要链接到服务器地址
			// cancleImg 图片显示		
			'cancelImg': context_ + '/image/uploadify/uploadify-cancel.png',
			//按钮显示的文字
			'buttonText' : '上传图片',
			//显示的高度和宽度，默认 height 30；width 120
			//'height': 15,
			//'width': 80,
			//上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
			//在浏览窗口底部的文件类型下拉菜单中显示的文本
			'fileTypeDesc' : 'Image Files',
			//允许上传的文件后缀
			'fileTypeExts' : '*.gif; *.jpg; *.png',
			//发送给后台的其他参数通过formData指定
			//'formData': { 'someKey': 'someValue', 'someOtherKey': 1 },
			//上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
			//'queueID': 'fileQueue',
			//选择文件后自动上传
			'auto' : true,
			//设置为true将允许多文件上传
			'multi' : true,
			'onUploadSuccess' : function(file, data, response) {
				var myobj = eval('(' + data + ')');
				console.log(myobj.url);
			}
		});
	});
</script>
</head>

<body>
	<div>
		<%--用来作为文件队列区域--%>
		<div id="fileQueue"></div>
		<input type="file" name="uploadify" id="uploadify" />
	</div>

</body>
</html>