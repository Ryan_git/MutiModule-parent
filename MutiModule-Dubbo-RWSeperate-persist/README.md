#20151111
	创建Dubbo-RWSeperate-persist 层，此模块为测试Dubbo 分布式系统中，持久层相关模块；
	
		使用 mybatis-generator-maven-plugin 插件，反向映射实体类信息，
			扩展自定义的分页插件，此插件同时兼容oracle mysql两种数据库，采用逻辑为 mybatis generator databaseIdProvider _databaseId 的使用；
			主键默认为使用 ID 来标示主键，主键的生成，采用  IdWorkerInstance.getId() 生成默认的主键，解决分布式系统的主键生成策略问题；
	
#20151113
	修改数据库实例，创建  inspinia+ 数据库，用来单独抽离表结构进行 inspinia+ 的admin theme 测试；
	
#20151214
	增加SysmanResource类相关文件，同时修改mybaits.xml 配置项：
		使用mybatis延迟加载时，因为接口返回的是代理对象，导致dubbo序列化后属性值全部为null
		https://github.com/alibaba/dubbo/issues/156
		
#20151231
	generatorConfig.xml 文件 增加
		<columnOverride column="create_time" property="createTime" jdbcType="TIMESTAMP"/>  	
	配置；
		防止mysql 在使用过程中出现 精度缺失（时分秒）；
			并修复之前的sysmanUser sysmanRole sysmanResource 三张表结构存在的问题；
			修复后注意 mapper.xml 文件中 jdbcType 由 Date 类型  调整为 #{createTime,jdbcType=TIMESTAMP}  
			
#20160103
	SysmanResource 表结构 增加 levels(级别，第几层) parent_ids(父类ids字符串集合) 字段
	
#20160122
	持久层创建的表名约定， 表名的话，使用前缀标示此表结构属于哪个范围(sys_ 开头的表结构为系统表；……)
	列名的话使用   ‘_’ 下划线进行分隔，防止后期需要取出表结构字段时不能有效转换为驼峰式结构；
		使用 _ 分隔列名中的各个英文单词； 约定性配置；
	
	创建的表结构sql语句在执行过程中避免出现 ‘ 分隔符；
		解决不同数据库中的大写小区别问题；
			因为,Oracle创建表的一条规则为：在命名表的时候可以使用大写或小写字母。只要表名或字段名没有用双引号括住，Oracle 对大小写就不敏感。
			
#20160125
	sysmanResource 类中 parentId 字段修改为 Long 类型

#20160308
	增加 方法，用来判断此用户包含某个资源下的哪些权限。用来针对按钮级别权限部分进行控制；
	
#20160318
	mysql delete 操作，表结构别名问题；
		比如执行右侧的sql语句 ：   delete mutidatabase a
			是会报错的；
			
#20160420
	RWSeperate-persist模块：
		考虑后期的通用性代码部分，测试使用mybatis-generator 过程中不生成 Example 类的情况；
			并抽离一些通用代码部分（selectCountByMap  selectListByMap ……）方式方法； 并进行单元测试；
	使用Example 类的话，代码通用度过低，考虑进行测试，不再使用Example类；
	 
#20160421
	Dubbo-RWSeperate-* 模块：
			将 sys_*  后台 RABC 相关的表结构都进行处理，移除 Example 相关的表结构，改为自定义的通用方法；
				本地扩展mybatis-generator 插件，自定义插件解决基础功能处理；
				后续有新的业务逻辑，则重新扩展此自定义插件；
				
#20160531
	RWSeperate-persist模块：
		针对generatorConfig.xml配置 引入配置文件
		
#20160602
	RWSeperate-persist模块：
		针对generatorConfig.xml配置  增加基础bean的继承操作，将通用属性集成到BaseEntity内部
			javaModelGenerator 的 rootClass 属性
			
		针对generatorConfig.xml配置， 将属性部分完全抽离出来(mybatis-generator.properties);
			如果需要进行代码生成，则只需要修改此配置文件部分即可，与 XML 部分的配置完全隔离开来；
			
#20160615
	RWSeperate-persist模块：
		增加实体类结构的注释部分；generatorConfig.xml 中的 commentGenerator 属性部分，自定义一个本地扩展的java类；
			这个类复制粘贴 mybatis-generator-core-1.3.2.jar 内的 org.mybatis.generator.internal.DefaultCommentGenerator 文件， 修改点在  addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn)  方法中。 将数据库中的注释部分加载到实体类中；
			
#20160618
	省市区数据
		https://github.com/kakuilan/china_area_mysql
		
#20160622
	Dubbo-RWS*-persist:增加model类的自定义注解实现
		为了后续 数据权限的实现，在生成model类的过程中，增加自定义注解部分代码的生成