package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.interceptor.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.interceptor.constants.vo.SqlModelVO;

public class ConstantsSQLModelUtil {

	public static final Map<String, List<SqlModelVO>> operationMap = new HashMap<String, List<SqlModelVO>>();

	static {
		List<SqlModelVO> _operation = new ArrayList<SqlModelVO>();
		SqlModelVO _vo = new SqlModelVO("demo_attachment", "NAME", "like", "%1%");
		_operation.add(_vo);
		operationMap.put("com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload.DemoAttachmentMapper",
				_operation);
	}
}
