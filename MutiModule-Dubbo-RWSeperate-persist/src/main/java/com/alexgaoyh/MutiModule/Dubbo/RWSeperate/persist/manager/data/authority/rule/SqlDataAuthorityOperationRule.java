package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule;

import com.MutiModule.common.mybatis.annotation.*;
import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "sys_data_authority_rule", namespace = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRuleMapper", remarks = "数据权限-规则", aliasName = "sys_data_authority_rule sys_data_authority_rule")
public class SqlDataAuthorityOperationRule extends BaseEntity implements Serializable {
	/**
	 * 名称,所属表字段为sys_data_authority_rule.NAME
	 */
	@MyBatisColumnAnnotation(name = "NAME", value = "sys_data_authority_rule_NAME", chineseNote = "名称", tableAlias = "sys_data_authority_rule")
	private String name;

	/**
	 * 描述,所属表字段为sys_data_authority_rule.REMARKS
	 */
	@MyBatisColumnAnnotation(name = "REMARKS", value = "sys_data_authority_rule_REMARKS", chineseNote = "描述", tableAlias = "sys_data_authority_rule")
	private String remarks;

	/**
	 * 类路径,所属表字段为sys_data_authority_rule.CLASS_PATH
	 */
	@MyBatisColumnAnnotation(name = "CLASS_PATH", value = "sys_data_authority_rule_CLASS_PATH", chineseNote = "类路径", tableAlias = "sys_data_authority_rule")
	private String classPath;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getClassPath() {
		return classPath;
	}

	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", name=").append(name);
		sb.append(", remarks=").append(remarks);
		sb.append(", classPath=").append(classPath);
		sb.append("]");
		return sb.toString();
	}
}