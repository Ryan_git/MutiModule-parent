package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;

public interface SysmanResourceMapper {
	int deleteByPrimaryKey(String id);

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanResource> selectListByMap(Map<Object, Object> map);

	int insert(SysmanResource record);

	int insertSelective(SysmanResource record);

	SysmanResource selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(SysmanResource record);

	int updateByPrimaryKey(SysmanResource record);

	// alexgaoyh add begin
	List<TreeNode> selectTreeNodeBySysmanResourceId(String id);

	List<ZTreeNodes> selectAllResourceZTreeNode();

	List<SysmanResource> selectResourceListByUserId(String userId);

	/**
	 * 获取到所有 parent_id 为 null 的 资源集合
	 * 
	 * @return
	 */
	List<SysmanResource> selectSysmanResourceListByNullParentId();

	/**
	 * 根据当前用户 ID 与 当前资源的主键 id，获取到 用户包含的 权限集合
	 * 
	 * @param userId
	 * @param parentId
	 * @return
	 */
	List<SysmanResource> selectIconResourceByUserIdAndParentId(@Param("userId") String userId,
			@Param("parentId") String parentId);
	
    /**
     *  根据 parentId 查询出符合条件的资源
     * @param parentId	父节点 
     * @return
     */
    List<SysmanResource> selectSysmanResourceByParentId(String parentId);
}