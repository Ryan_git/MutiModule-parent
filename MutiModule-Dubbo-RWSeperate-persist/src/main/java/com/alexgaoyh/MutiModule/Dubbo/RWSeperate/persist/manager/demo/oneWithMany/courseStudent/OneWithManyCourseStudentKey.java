package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class OneWithManyCourseStudentKey extends BaseEntity implements Serializable {
    private String demoOnewithmanyCourseId;

    private String demoOnewithmanyStudentId;

    private static final long serialVersionUID = 1L;

    public String getDemoOnewithmanyCourseId() {
        return demoOnewithmanyCourseId;
    }

    public void setDemoOnewithmanyCourseId(String demoOnewithmanyCourseId) {
        this.demoOnewithmanyCourseId = demoOnewithmanyCourseId;
    }

    public String getDemoOnewithmanyStudentId() {
        return demoOnewithmanyStudentId;
    }

    public void setDemoOnewithmanyStudentId(String demoOnewithmanyStudentId) {
        this.demoOnewithmanyStudentId = demoOnewithmanyStudentId;
    }
}