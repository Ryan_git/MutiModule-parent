package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.dict.dictionary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.mybatis.pagination.Page;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionaryMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;

public class DictDictionaryTest {

	private DictDictionaryMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DictDictionaryMapper) ctx.getBean( "dictDictionaryMapper" );
        
    }
    
	// @Test
    public void selectCountByMapTest() {
    	Map<Object, Object> map = new HashMap<Object, Object>();
    	map.put("myLike_content", 1);
    	mapper.selectCountByMap(map);
    }
	
	// @Test
	public void selectDictionaryWithItemsByMapTest() {
		
		String currentPageStr = "1";
		
		String recordPerPageStr = "10";

		int beginInt = Integer.parseInt(currentPageStr) >=1 ? Integer.parseInt(currentPageStr) : 1;
		
		Page page = new Page((beginInt - 1)*Integer.parseInt(recordPerPageStr), Integer.parseInt(recordPerPageStr));
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("page", page);
		
		List<DictDictionaryWithItemView> list = mapper.selectDictionaryWithItemsByMap(map);
		System.out.println("selectDictionaryWithItemsByMapTest = " + list.size());
	}
	
}
