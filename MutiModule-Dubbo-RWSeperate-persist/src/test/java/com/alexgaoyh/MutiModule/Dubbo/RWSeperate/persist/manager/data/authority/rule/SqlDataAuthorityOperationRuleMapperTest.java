package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.mybatis.pagination.Page;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

public class SqlDataAuthorityOperationRuleMapperTest {

	private SqlDataAuthorityOperationRuleMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (SqlDataAuthorityOperationRuleMapper) ctx.getBean( "sqlDataAuthorityOperationRuleMapper" );
        
    }
	
	// @Test
	public void selectSingleZTreeNodeListByParentIdTest() {
		Map<Object , Object> map = new HashMap<Object, Object>();
		map.put("id", 22L);
		Page page = new Page(0, 10);
		map.put("page", page);
		
		List<SqlDataAuthorityOperationView> list = mapper.selectListWithOperationModelByMap(map);
		if(list != null) {
			for (SqlDataAuthorityOperationView sqlDataAuthorityOperationView : list) {
				System.out.println(sqlDataAuthorityOperationView.getId());
			}
		}
	}
}
