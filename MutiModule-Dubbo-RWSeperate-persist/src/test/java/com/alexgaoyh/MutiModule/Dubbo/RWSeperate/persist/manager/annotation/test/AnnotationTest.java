package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.annotation.test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.MutiModule.common.mybatis.annotation.MyBatisColumnAnnotation;
import com.MutiModule.common.mybatis.annotation.MyBatisTableAnnotation;
import com.MutiModule.common.utils.ClassUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo.SqlDataAuthorityModelVO;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo.SqlDataAuthorityRuleVO;

public class AnnotationTest {

	// @Test
	public void scanPackageWithAnnotation() {
		List<SqlDataAuthorityRuleVO> _ruleVOlist = new ArrayList<SqlDataAuthorityRuleVO>();

		Set<Class<?>> classSet = ClassUtilss.scanPackageByAnnotation(
				"com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist", MyBatisTableAnnotation.class);
		for (Class<?> class1 : classSet) {
			MyBatisTableAnnotation annotationTable = class1.getAnnotation(MyBatisTableAnnotation.class);
			if (annotationTable == null) {
				continue;
			} else {
				SqlDataAuthorityRuleVO _ruleVO = new SqlDataAuthorityRuleVO(class1.getName(),
						annotationTable.remarks());

				Field[] field = class1.getDeclaredFields();

				if (field != null && field.length > 0) {

					List<SqlDataAuthorityModelVO> _modelVOList = new ArrayList<SqlDataAuthorityModelVO>();

					for (Field field2 : field) {
						MyBatisColumnAnnotation annotationColumn = field2.getAnnotation(MyBatisColumnAnnotation.class);
						if (annotationColumn != null) {
							SqlDataAuthorityModelVO _modelVO = new SqlDataAuthorityModelVO(field2.getName(),
									annotationColumn.chineseNote());
							_modelVOList.add(_modelVO);
						}
					}
					_ruleVO.setItems(_modelVOList);
				}
				_ruleVOlist.add(_ruleVO);
			}
		}
	}
}
