/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : inspinia+

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2016-03-21 11:03:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `demo_onewithmany_course`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_course`;
CREATE TABLE `demo_onewithmany_course` (
  `ID` bigint(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_course
-- ----------------------------
INSERT INTO `demo_onewithmany_course` VALUES ('11105422174921728', '0', '2016-03-22 18:37:49', 'classname-------');
INSERT INTO `demo_onewithmany_course` VALUES ('11121085862126592', '0', '2016-03-22 01:01:17', '1');

-- ----------------------------
-- Table structure for `demo_onewithmany_course_student_rel`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_course_student_rel`;
CREATE TABLE `demo_onewithmany_course_student_rel` (
  `DEMO_ONEWITHMANY_COURSE_ID` bigint(32) NOT NULL DEFAULT '0',
  `DEMO_ONEWITHMANY_STUDENT_ID` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DEMO_ONEWITHMANY_COURSE_ID`,`DEMO_ONEWITHMANY_STUDENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_course_student_rel
-- ----------------------------
INSERT INTO `demo_onewithmany_course_student_rel` VALUES ('11105422174921728', '11121080921433088');
INSERT INTO `demo_onewithmany_course_student_rel` VALUES ('11105422174921728', '11121081735652352');
INSERT INTO `demo_onewithmany_course_student_rel` VALUES ('11121085862126592', '11121085868221440');
INSERT INTO `demo_onewithmany_course_student_rel` VALUES ('11121085862126592', '11121086664156160');

-- ----------------------------
-- Table structure for `demo_onewithmany_student`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_student`;
CREATE TABLE `demo_onewithmany_student` (
  `ID` bigint(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_student
-- ----------------------------
INSERT INTO `demo_onewithmany_student` VALUES ('11105422174921729', '0', '2016-03-20 10:38:58', '0-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105422183244800', '0', '2016-03-20 10:38:58', '1-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105422183572480', '0', '2016-03-20 10:38:58', '2-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105426726855680', '0', '2016-03-20 10:38:58', '3-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105491290629120', '0', '2016-03-20 10:55:23', '4-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105491290825728', '0', '2016-03-20 10:55:23', '5-');
INSERT INTO `demo_onewithmany_student` VALUES ('11105492914545664', '0', '2016-03-19 20:55:48', '6-');
INSERT INTO `demo_onewithmany_student` VALUES ('11121013037016064', '0', '2016-03-21 10:42:46', '7-');
INSERT INTO `demo_onewithmany_student` VALUES ('11121013039375360', '0', '2016-03-22 00:42:46', '7-');
INSERT INTO `demo_onewithmany_student` VALUES ('11121080919532544', '0', '2016-03-21 11:00:02', 'q');
INSERT INTO `demo_onewithmany_student` VALUES ('11121080921170944', '0', '2016-03-21 11:00:02', 'w');
INSERT INTO `demo_onewithmany_student` VALUES ('11121080921433088', '0', '2016-03-22 15:00:02', 's');
INSERT INTO `demo_onewithmany_student` VALUES ('11121081735652352', '0', '2016-03-22 01:00:15', '1');
INSERT INTO `demo_onewithmany_student` VALUES ('11121085862126593', '0', '2016-03-21 11:01:17', 'q');
INSERT INTO `demo_onewithmany_student` VALUES ('11121085862388736', '0', '2016-03-21 11:01:17', 'w');
INSERT INTO `demo_onewithmany_student` VALUES ('11121085862650880', '0', '2016-03-21 11:01:17', 'e');
INSERT INTO `demo_onewithmany_student` VALUES ('11121085868221440', '0', '2016-03-22 01:01:17', 'r');
INSERT INTO `demo_onewithmany_student` VALUES ('11121086664156160', '0', '2016-03-21 11:01:30', '1');

-- ----------------------------
-- Table structure for `mutidatabase`
-- ----------------------------
DROP TABLE IF EXISTS `mutidatabase`;
CREATE TABLE `mutidatabase` (
  `id` bigint(50) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mutidatabase
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_sysmanresource`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysmanresource`;
CREATE TABLE `sys_sysmanresource` (
  `ID` bigint(32) NOT NULL AUTO_INCREMENT,
  `DELETE_FLAG` varchar(50) DEFAULT NULL,
  `CREATE_TIME` timestamp NULL DEFAULT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `HREF` varchar(100) DEFAULT NULL,
  `RESOURCE_TYPE` int(1) DEFAULT NULL,
  `PARENT_ID` bigint(32) DEFAULT NULL,
  `LEVELS` int(1) DEFAULT NULL,
  `PARENT_IDS` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11105064893752321 DEFAULT CHARSET=utf8 COMMENT='后台资源权限表';

-- ----------------------------
-- Records of sys_sysmanresource
-- ----------------------------
INSERT INTO `sys_sysmanresource` VALUES ('11048197823472640', '0', '2016-03-08 14:04:54', '系统管理', '系统管理', '#', '0', null, '0', '');
INSERT INTO `sys_sysmanresource` VALUES ('11048198557017088', '0', '2016-03-08 14:05:05', '权限管理', '权限管理', '#', '0', '11048197823472640', '1', '11048197823472640,');
INSERT INTO `sys_sysmanresource` VALUES ('11048199436444672', '0', '2016-03-08 14:05:19', '后台用户管理', '后台用户管理', 'manager/sysman/sysmanUser/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysmanresource` VALUES ('11048200350082048', '0', '2016-03-08 14:05:33', '后台用户组管理', '后台用户组管理', 'manager/sysman/sysmanRole/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysmanresource` VALUES ('11048201254872064', '0', '2016-03-08 14:05:47', '后台资源管理', '后台资源管理', 'manager/sysman/sysmanResource/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysmanresource` VALUES ('11048209978500096', '0', '2016-03-08 14:08:00', '添加', '后台用户管理-添加', 'manager/sysman/sysmanUser/add', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysmanresource` VALUES ('11048211508175872', '0', '2016-03-08 14:08:23', '修改', '后台用户管理-修改', 'manager/sysman/sysmanUser/edit', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysmanresource` VALUES ('11048212395664384', '0', '2016-03-08 14:08:37', '角色', '后台用户管理-角色', 'manager/sysman//sysmanUserRoleRel/userRoleRelList', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysmanresource` VALUES ('11048249680208896', '0', '2016-03-08 14:18:06', '添加', '后台用户组管理-添加', 'manager/sysman/sysmanRole/add', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysmanresource` VALUES ('11048250523788288', '0', '2016-03-08 14:18:18', '修改', '后台用户组管理-修改', 'manager/sysman/sysmanRole/edit', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysmanresource` VALUES ('11048251408131072', '0', '2016-03-08 14:18:32', '资源', '后台用户组管理-资源', 'manager/sysman/sysmanRoleResourceRel/roleResourceRelList', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysmanresource` VALUES ('11048268803679232', '0', '2016-03-08 14:22:57', '添加', '后台资源管理-添加', 'manager/sysman/sysmanResource/add', '1', '11048201254872064', '3', '11048197823472640,11048198557017088,11048201254872064,');
INSERT INTO `sys_sysmanresource` VALUES ('11048270217618432', '0', '2016-03-08 14:23:19', '修改', '后台资源管理-修改', 'manager/sysman/sysmanResource/edit', '1', '11048201254872064', '3', '11048197823472640,11048198557017088,11048201254872064,');
INSERT INTO `sys_sysmanresource` VALUES ('11087578173547520', '0', '2016-03-15 12:59:51', '测试功能', '测试功能', '#', '0', null, '0', '');
INSERT INTO `sys_sysmanresource` VALUES ('11087579158619136', '0', '2016-03-15 13:00:06', '一对多添加', '一对多添加', 'manager/demo/oneWithMany/courseStudent/list', '0', '11087578173547520', '1', '11087578173547520,');
INSERT INTO `sys_sysmanresource` VALUES ('11099611938563072', '0', '2016-03-17 16:00:11', '添加', '测试功能-一对多添加-添加按钮', 'manager/demo/oneWithMany/courseStudent/add', '1', '11087579158619136', '2', '11087578173547520,11087579158619136,');
INSERT INTO `sys_sysmanresource` VALUES ('11105064893752320', '0', '2016-03-18 15:06:57', '修改', '测试功能-一对多添加-修改', 'manager/sysman/sysmanResource/edit', '1', '11087579158619136', '2', '11087578173547520,11087579158619136,');

-- ----------------------------
-- Table structure for `sys_sysmanrole`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysmanrole`;
CREATE TABLE `sys_sysmanrole` (
  `ID` bigint(32) NOT NULL,
  `DELETE_FLAG` varchar(50) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NAME` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Records of sys_sysmanrole
-- ----------------------------
INSERT INTO `sys_sysmanrole` VALUES ('682752188681949184', '0', '2016-01-03 11:52:29', '系统管理员', '系统管理员-所有权限用户组');

-- ----------------------------
-- Table structure for `sys_sysmanuser`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysmanuser`;
CREATE TABLE `sys_sysmanuser` (
  `ID` bigint(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NAME` varchar(32) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户表结构';

-- ----------------------------
-- Records of sys_sysmanuser
-- ----------------------------
INSERT INTO `sys_sysmanuser` VALUES ('682814769132081152', '0', '2016-01-28 11:55:49', 'admin@admin.com', 'admin@admin.com');

-- ----------------------------
-- Table structure for `sys_sysman_role_resource_rel`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_role_resource_rel`;
CREATE TABLE `sys_sysman_role_resource_rel` (
  `SYSMAN_ROLE_ID` bigint(32) NOT NULL DEFAULT '0',
  `SYSMAN_RESOURCE_ID` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SYSMAN_ROLE_ID`,`SYSMAN_RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sysman_role_resource_rel
-- ----------------------------
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048197823472640');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048198557017088');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048199436444672');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048200350082048');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048201254872064');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048209978500096');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048211508175872');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048212395664384');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048249680208896');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048250523788288');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048251408131072');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048268803679232');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11048270217618432');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11087578173547520');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11087579158619136');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11099611938563072');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('682752188681949184', '11105064893752320');

-- ----------------------------
-- Table structure for `sys_sysman_user_role_rel`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_user_role_rel`;
CREATE TABLE `sys_sysman_user_role_rel` (
  `SYSMAN_USER_ID` bigint(32) NOT NULL DEFAULT '0',
  `SYSMAN_ROLE_ID` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SYSMAN_USER_ID`,`SYSMAN_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sysman_user_role_rel
-- ----------------------------
INSERT INTO `sys_sysman_user_role_rel` VALUES ('682814769132081152', '682752188681949184');
