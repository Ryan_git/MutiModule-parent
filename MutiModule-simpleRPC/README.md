#20160307
	这是一个简单的RPC 调用测试，代码来源于 http://javatar.iteye.com/blog/1123915
		动态代理在RPC 框架中的使用，包含socket 编程
			服务器启动了一个线程监听 Socket 端口, 有Socket访问了, 反序列化解析出调用哪个Service 哪个 方法, 以及传入的 参数,再用Socket 写回去.
			客户端 利用 Jdk 的Proxy 生成了一个代理类,在创建 Proxy 时建立与服务器的Socket连接.调用 Proxy 的方法时, 向服务器发送数据, 等待结果返回.
		
		1.服务端 接受客户端来的socket流， 接受约定为
	     1.1 方法名
	     1.2 参数类型
	     1.3 方法所需参数
		2  客户端动态代理生成 代理service,调用该service的方法实则 交给invoke方法处理