package com.MutiModule.simpleRPC;

import com.MutiModule.simpleRPC.demo.IHelloService;

/**
 * 引用服务
 * @author alexgaoyh
 *
 */
public class RpcConsumer {

	public static void main(String[] args) throws Exception {
		IHelloService service = RpcFramework.refer(IHelloService.class, "127.0.0.1", 1234);
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			String hello = service.hello("World" + i);
			System.out.println(hello);
			Thread.sleep(1000);
		}
	}
}
