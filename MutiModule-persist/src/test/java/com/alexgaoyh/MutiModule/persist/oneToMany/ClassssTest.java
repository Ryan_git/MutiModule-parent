package com.alexgaoyh.MutiModule.persist.oneToMany;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.alexgaoyh.MutiModule.persist.oneToMany.classss.Classss;
import com.alexgaoyh.MutiModule.persist.oneToMany.classss.ClassssMapper;

public class ClassssTest {
	
	private ClassssMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (ClassssMapper) ctx.getBean( "classssMapper" );
        
    }
	
	//@Test
	public void insertTest() {
		try {
			Classss cla = new Classss();
			cla.setDeleteFlag(DeleteFlagEnum.NORMAL);
			cla.setCreateTime(new Date());
			cla.setNamess("班级1");
			mapper.insert(cla);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
