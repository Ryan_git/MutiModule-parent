package com.alexgaoyh.MutiModule.persist.demo;

public enum EnumEntityTest {

	INSERT("0","插入"), 
	DELETE("1","删除"), 
	UPDATE("2","更新"), 
	SELECT("3","选择");
	
	private String code;
	private String name;
	
	private EnumEntityTest(String code, String name){
		this.code = code;
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
