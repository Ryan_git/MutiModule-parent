package com.alexgaoyh.ssoDemo1.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.MutiModule.common.utils.SSOPropertiesUtilss;
import com.MutiModule.common.utils.StaticUrlPropertiesUtilss;

public class UserCenterServlet extends HttpServlet{

	/**
	 * 登陆页面
	 */
	private static String SSOLoginPage;
	
	/**
	 * 保存的cookie名称部分
	 */
	private static String CookieName;

	/**
	 * 保存的cookie路径部分，cookie.setPath()
	 */
	private static String DomainName;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		SSOLoginPage = SSOPropertiesUtilss.getSSOLoginPage();
		CookieName = SSOPropertiesUtilss.getCookieName();
		DomainName = SSOPropertiesUtilss.getDomainName();

		Cookie[] cookies=   request.getCookies();
		
		Cookie loginCookie =null;
		
		String cookname ="";
		
		if(cookies!=null){
		
		    for(Cookie cookie:cookies){
		
		        cookname =cookie.getName().trim().toLowerCase();
		       if(CookieName.equals(cookname)){
		           loginCookie =cookie;
		           break;
		       }
		
		    }
		
		}
		if(loginCookie==null){
		    String url =request.getRequestURL().toString();
		    response.sendRedirect(SSOLoginPage+"?goto="+url);
		} else {
			request.setAttribute("staticUrl", StaticUrlPropertiesUtilss.getStaticUrl());
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}
}
