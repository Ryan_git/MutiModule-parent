package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect;

import java.util.List;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityClassAnnotation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo.DictDictionaryVOUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo.SqlModelVO;

/**
 * 数据权限（行数据权限） 部分的实现
 * @author alexgaoyh
 *
 */
@Aspect
public class DataAuthorityAspect {

	/**
	 * 数据权限的AOP拦截，必须满足如下条件：
	 * 	1：service层实现类部分的实现类中必须使用 @DataAuthorityClassAnnotation 注解；
	 *  2：service层实现类具体需要拦截的方法中，必须使用 @DataAuthorityMethodAnnotation 注解；
	 *  3：service层的接口类部分需要以 @DataAuthorityCurrSysUseLoginAnnotation 注释的java.lang.String 数据类型为开头部分
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	@Around("@annotation(com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityMethodAnnotation)"
			+ "&& execution(* *(@com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityCurrSysUseLoginAnnotation (*),..))")
	public Object dataAuthorityAnnotation(ProceedingJoinPoint pjp) throws Throwable {

		Object[] inputObjectArray = pjp.getArgs();
		
		// 获取到这个类上的注解
		DataAuthorityClassAnnotation anns = pjp.getTarget().getClass()
				.getAnnotation(DataAuthorityClassAnnotation.class);
		
		// 判断这个类上面的注释是否是AnnotationName这个自定义的注解，如果是返回这个注解，如果不是返回null
		if (anns != null) {

			System.out.println("class name: " + pjp.getSignature().getDeclaringType().getName());
			System.out.println("method name: " + pjp.getSignature().getName());

			// 输出这个类上的注解的值
			System.out.println("注释在实现类上的annotation：" + anns.className());
			
			for (int i = 0; i < inputObjectArray.length; i++) {
				Object _obj = inputObjectArray[i];
				if (_obj instanceof java.lang.String) {
					System.out.println("_obj.toString() = " + _obj.toString());
				}
			}

			for (int i = 0; i < inputObjectArray.length; i++) {
				Object _obj = inputObjectArray[i];
				System.out.println("parameters: " + _obj);
				if (_obj instanceof Map) {
					Map<Object, Object> map = (Map<Object, Object>) _obj;
					List<SqlModelVO> _sqlModelVOList = DictDictionaryVOUtilss.operationMap.get(anns.className());
					if (_sqlModelVOList != null && _sqlModelVOList.size() > 0) {
						for (SqlModelVO sqlModelVO : _sqlModelVOList) {
							map.put(sqlModelVO.getProp(), sqlModelVO.getValues());
						}
						inputObjectArray[i] = map;
					}
				}
			}
			System.out.println("end of exec function");
		}

		Object result = pjp.proceed(inputObjectArray);

		return result;
	}

}
