package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 将项目打包为一个可执行jar，并且注册相关服务提供者
 * @author alexgaoyh
 *
 */
public class MainJarUtilss {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		//可以加载多个配置文件
        String[] springConfigFiles = {"Dubbo.xml","mybatis-spring-config.xml" };
        
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.setConfigLocations(springConfigFiles);
		context.refresh();
		
		context.start();
		System.in.read(); // 按任意键退出
	}
}
