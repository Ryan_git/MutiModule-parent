package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.sysman.sysmanResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.MutiModule.common.vo.enums.MenuResourceTypeEnum;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResourceMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.tree.TreeConvert;

@Service(value = "sysmanResourceService")
public class SysmanResourceServiceImpl implements ISysmanResourceReadService {

	@Resource(name = "sysmanResourceMapper")
	private SysmanResourceMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SysmanResource> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public SysmanResource selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public List<TreeNode> selectTreeNodeBySysmanResourceId(String id) {
		return mapper.selectTreeNodeBySysmanResourceId(id);
	}

	@Override
	public List<ZTreeNodes> selectAllResourceZTreeNode() {
		return mapper.selectAllResourceZTreeNode();
	}

	@Override
	public List<SysmanResource> selectResourceListByUserId(String userId) {
		return mapper.selectResourceListByUserId(userId);
	}

	@Override
	public List<SysmanResource> selectSysmanResourceListByNullParentId() {
		return mapper.selectSysmanResourceListByNullParentId();
	}

	@Override
	public List<SysmanResource> selectIconResourceByUserIdAndParentId(String userId, String parentId) {
		return mapper.selectIconResourceByUserIdAndParentId(userId, parentId);
	}

	@Override
	public MyPageView<SysmanResource> generateMyPageViewVO(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);

		List<SysmanResource> _list = mapper.selectListByMap(map);

		int recordPerPage = 10;
		int currentPage = 1;
		if (map.get("page") != null) {
			Object _pageObj = map.get("page");
			if (_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page) _pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SysmanResource> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage,
				_totalCount, _list);

		return pageView;
	}

	@Override
	public List<String> selectParentIdsByPrimaryKey(String id, List<String> list) {
		SysmanResource sResource = mapper.selectByPrimaryKey(id);
		if (sResource != null) {
			if (null != sResource.getParentId()) {
				list.add(0, sResource.getParentId());
				selectParentIdsByPrimaryKey(sResource.getParentId(), list);
			}
		}
		return list;
	}

	@Override
	public List<TreeNode> selectTreeNodeListByUserId(String userId) {
		List<SysmanResource> allNodesList = mapper.selectResourceListByUserId(userId);

		List<SysmanResource> rootNodesList = TreeConvert.getRootNodesList(allNodesList);

		List<TreeNode> treeNodeList = TreeConvert.convertSysmanResourceListToTreeNodeList(rootNodesList, allNodesList);

		return treeNodeList;
	}

	@Override
	public List<SysmanResource> selectSysmanResourceByParentId(String parentId) {
		return mapper.selectSysmanResourceByParentId(parentId);
	}

	@Override
	public List<LinkedHashSet<String>> selectRootToLeafNodePathStrList() {
		List<LinkedHashSet<String>> leafNodeToRootStrList = new ArrayList<LinkedHashSet<String>>();
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("resourceType", MenuResourceTypeEnum.BUTTON);
		List<SysmanResource> buttonSysmanResource = mapper.selectListByMap(map);
		if(buttonSysmanResource != null) {
			for (SysmanResource sysmanResource : buttonSysmanResource) {
				String _parentIds = sysmanResource.getParentIds();
				if(StringUtilss.isNotEmpty(_parentIds)) {
					LinkedHashSet<String> _linkedStrSet = new LinkedHashSet<String>();
					String[] _parentIdsArray = _parentIds.split(",");
					for (int i = 0; i < _parentIdsArray.length; i++) {
						if(StringUtilss.isNotEmpty(_parentIdsArray[i])) {
							_linkedStrSet.add(_parentIdsArray[i]);
						}
					}
					_linkedStrSet.add(sysmanResource.getId());
					leafNodeToRootStrList.add(_linkedStrSet);
				}
			}
			return leafNodeToRootStrList;
		} else {
			return null;
		}
	}

	@Override
	public int selectMaxDepthFromSelectedNodeToLeaf(String id) {
		int maxDepthNumber = 0;
		List<LinkedHashSet<String>> setList = this.selectRootToLeafNodePathStrList();
		if(setList != null && setList.size() > 0 ) {
			for (LinkedHashSet<String> linkedHashSet : setList) {
				int _i = 0;
				for (String string : linkedHashSet) {
					_i++;
					if(string.equals(id)) {
						int _index = _i;
						int _depth = linkedHashSet.size() - _index;
						if(_depth > maxDepthNumber) {
							maxDepthNumber = _depth;
						}
					}
				}
			}
		}
		return maxDepthNumber;
	}

}
