package com.MutiModule.alexgaoyh.upload.constants;

import java.io.InputStream;
import java.util.Properties;

public class CORSConstants {

	private static String Access_Control_Allow_Origin;
	
	static{
		
        loads();
        
    }
	
    synchronized static public void loads(){
    	
        if(null == Access_Control_Allow_Origin || "".equals(Access_Control_Allow_Origin)){
            InputStream is = CORSConstants.class.getResourceAsStream("/cors.properties");
            Properties dproperties = new Properties();
            try {
            	dproperties.load(is);
                        
            	Access_Control_Allow_Origin = dproperties.getProperty("CORSFilter").toString();
            	
            }
            catch (Exception e) {
                System.err.println("不能读取属性文件. " + "请确保 cors.properties 在CLASSPATH指定的路径中");
            }
        }
        
    }

	public static String getAccess_Control_Allow_Origin() {
		return Access_Control_Allow_Origin;
	}
    
    
}
