package com.MutiModule.alexgaoyh.upload.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;

public class FileUploadServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//文件保存目录路径
		String savePath = req.getServletContext().getRealPath("/") + File.separator + "uploadFiles" + File.separator;

		//文件保存目录URL
		String saveUrl  = req.getContextPath() + File.separator + "uploadFiles" + File.separator;
		
		// 20150717	alexgaoyh	servlet上传时，增加文件路径部分的限定 	文件空间功能处理	begin
		String contextPath = req.getParameter("contextPath");
		String detailpath = req.getParameter("detailPath");
		if(contextPath != null && contextPath != "") {
			savePath = savePath + contextPath + File.separator;
			saveUrl = saveUrl + contextPath + File.separator;
		}
		if(detailpath != null && detailpath != "") {
			savePath = savePath + detailpath + File.separator;
			saveUrl = saveUrl + detailpath + File.separator;
		}
		// 20150717	alexgaoyh	servlet上传时，增加文件路径部分的限定 	文件空间功能处理	end

		try {
			req.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			System.out.println(e1.getMessage());
		}

		File file = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String ymd = sdf.format(new Date());
		savePath += ymd + "/";
		saveUrl += ymd + "/";
		File dirFile = new File(savePath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}

		String fileName = (String) req.getParameter("name");
		
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
		
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
		
		File uploadedFile = new File(savePath, newFileName);

		FileOutputStream out = null;

		try {
			out = new FileOutputStream(uploadedFile);
			
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("url", saveUrl + newFileName);
			resp.getWriter().println(obj.toJSONString());
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		try {
			IOUtils.copy(req.getInputStream(), out);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IOUtils.closeQuietly(out);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	private String getError(String message) {
		JSONObject obj = new JSONObject();
		obj.put("error", 1);
		obj.put("message", message);
		return obj.toJSONString();
	}

}
