package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.demo.ip2region;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.mybatis.base.BaseEntity;
import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.ip2region.IP2RegionUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.util.constants.ip2region.IP2RegionConstantUtilss;

@Controller
@RequestMapping(value="manager/demo/ip2region")
public class IP2RegionController extends BaseController<BaseEntity>{

	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {

		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		
		model.setViewName(moduleName + "/list");
		
		return model;
	}
	
	@RequestMapping(value = "/getRegionByIP", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String getZtreeNodesListById(HttpServletRequest requestInput) {
		String ip = requestInput.getParameter("ipAddress");
		String regionStr = IP2RegionUtilss.ip2RegionMethod(ip, IP2RegionConstantUtilss.ip2RegionDBFilePath);
		return JSONUtilss.toJSon(regionStr);
	}
	
	
}
