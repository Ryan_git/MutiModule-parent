package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.demo.upload;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload.DemoAttachment;

@Controller
@RequestMapping(value="manager/demo/uploadTest")
public class UploadTestController extends BaseController<DemoAttachment>{

	
}
