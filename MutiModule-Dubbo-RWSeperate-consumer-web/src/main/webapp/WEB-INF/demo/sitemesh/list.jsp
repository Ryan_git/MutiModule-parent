<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
    <!-- Data Tables -->
    <link href="/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
    
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Basic Data Tables example with responsive plugin</h5>
				<div class="ibox-tools">
					<a class="collapse-link binded"> <i class="fa fa-chevron-up"></i>
					</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i
						class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a></li>
						<li><a href="#">Config option 2</a></li>
					</ul>
					<a class="close-link binded"> <i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">

				<div id="DataTables_Table_0_wrapper"
					class="dataTables_wrapper form-inline">
					<div class="DTTT_container">
						<a class="DTTT_button DTTT_button_copy" title="View copy view" tabindex="0" aria-controls="DataTables_Table_0"><span>Copy</span></a>
						<a class="DTTT_button DTTT_button_xls" title="View xls view" tabindex="0" aria-controls="DataTables_Table_0"><span>Excel</span> </a>
						<a class="DTTT_button DTTT_button_pdf" title="View pdf view" tabindex="0" aria-controls="DataTables_Table_0"><span>PDF</span></a>
						<a class="DTTT_button DTTT_button_print" title="View print view" tabindex="0" aria-controls="DataTables_Table_0"><span>Print</span></a>
					</div>
					<div class="clear"></div>
					
					<c:import url="../../common/dataTable/myTableLengthJSP.jsp"></c:import>
					
					<div id="DataTables_Table_0_filter" class="dataTables_filter">
						<label>Search:<input type="search"
							class="form-control input-sm" placeholder=""
							aria-controls="DataTables_Table_0"></label>
					</div>
					<table
						class="table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline"
						id="DataTables_Table_0" role="grid"
						aria-describedby="DataTables_Table_0_info">
						<thead>
							<tr role="row">
								<th rowspan="1" colspan="1">Rendering engine</th>
								<th rowspan="1" colspan="1">Browser</th>
								<th rowspan="1" colspan="1">Platform(s)</th>
								<th rowspan="1" colspan="1">Engine version</th>
								<th rowspan="1" colspan="1">CSS grade</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${pageView.records}" var="data" varStatus="loop">
								<tr <c:if test="${loop.index%2 == 0}"> class = "gradeA odd "</c:if><c:if test="${loop.index%2 == 0}"> class = "gradeA even "</c:if> role="row">
									<td class="sorting_1">${data.id }</td>
									<td>${data.name }</td>
									<td>${data.id }</td>
									<td class="center">${data.name }</td>
									<td class="center">${data.id }</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">Rendering engine</th>
								<th rowspan="1" colspan="1">Browser</th>
								<th rowspan="1" colspan="1">Platform(s)</th>
								<th rowspan="1" colspan="1">Engine version</th>
								<th rowspan="1" colspan="1">CSS grade</th>
							</tr>
						</tfoot>
					</table>
					<!-- 将分页JSP包含进来 -->
					<%@ include file="../../common/dataTable/myPageJSP.jsp"%>
				</div>

			</div>
		</div>
	</div>
	
</body>
</html>