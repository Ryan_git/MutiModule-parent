package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.student.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;

/**
 * OneWithManyStudent 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyStudentReadService {

    int selectCountByMap(Map<Object, Object> map);

    List<OneWithManyStudent> selectListByMap(Map<Object, Object> map);

    OneWithManyStudent selectByPrimaryKey(String id);

    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<OneWithManyStudent> generateMyPageViewVO(Map<Object, Object> map);
    
}
