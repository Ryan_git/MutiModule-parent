package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;

/**
 * SysmanRoleResourceRel 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISysmanRoleResourceRelReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanRoleResourceRelKey> selectListByMap(Map<Object, Object> map);
    
    /**
     * 获取分页实体信息部分
     * @param exampleWithOutPage	参数传递，封装部分过滤参数
     * @param page	实体分页类部分
     * @return
     */
    MyPageView<SysmanRoleResourceRelKey> generateMyPageViewVO(Map<Object, Object> map);
    
}
