package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.model.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;

/**
 * SqlDataAuthorityOperationModel 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationModelReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationModel> selectListByMap(Map<Object, Object> map);

	SqlDataAuthorityOperationModel selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<SqlDataAuthorityOperationModel> generateMyPageViewVO(Map<Object, Object> map);
    
}
