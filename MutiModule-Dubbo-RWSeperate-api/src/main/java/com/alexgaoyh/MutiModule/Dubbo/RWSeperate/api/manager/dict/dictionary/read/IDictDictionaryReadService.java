package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityCurrSysUseLoginAnnotation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithZtreeNodeView;

/**
 * DictDictionary 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IDictDictionaryReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<DictDictionary> selectListByMap(Map<Object, Object> map);

	DictDictionary selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<DictDictionary> generateMyPageViewVO(Map<Object, Object> map);
    
    /**
     * 获取分页实体信息部分
     * @param currentSysmanUserLoginId	允许为空，为空的话则调用单参数的方法
     * 		当前系统登陆用户ID，用来获取当前系统登陆用户的信息--数据权限控制会使用
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<DictDictionary> generateMyPageViewVO(@DataAuthorityCurrSysUseLoginAnnotation(value = "currentSysmanUserLoginId") String currentSysmanUserLoginId, Map<Object, Object> map);
    
    /**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合）
	 * 多方为 DictDictionaryItem 类属性集合
	 * @param map  key部分为DictDictionary类的属性 
	 * @return
	 */
	List<DictDictionaryWithItemView> selectDictionaryWithItemsByMap(Map<Object, Object> map);
	
	/**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合）
	 * 多方为 ZTreeNodes 类属性集合
	 * @param map  key部分为DictDictionary类的属性 
	 * @return
	 */
	List<DictDictionaryWithZtreeNodeView> selectDictionaryWithZTreeNodesListByMap(Map<Object, Object> map);
    
}
