package com.MutiModule.common.dict;

import java.util.Map;

import com.MutiModule.common.utils.PropertiesUtilss;

/**
 * 错误编码
 * 	读取配置文件内的错误编码集合，并转换为map集合，形成错误编码部分
 * @author alexgaoyh
 *
 */
public class ErrorCodeDict {
	
	// 错误来源的map 集合，读取配置文件内的数据，并转换为map结合
	public static final Map<String, String> SOURCE_FROM_MAP = PropertiesUtilss.getMapByProperties("/dict/errorCode/source-from.properties");
	
	// 哪个服务提供者的错误集合 map， 读取配置文件内的数据，并转换为map集合
	public static final Map<String, String> SERVICE_NAME_MAP = PropertiesUtilss.getMapByProperties("/dict/errorCode/service-name.properties");
	
	// 错误类型 大类别的集合map， 读取配置文件内的数据，并转换为map集合
	public static final Map<String ,String> ERROR_TYPE_MAP = PropertiesUtilss.getMapByProperties("/dict/errorCode/error-type.properties");
	
	// 错误明细 对应的集合map 读取配置文件内的数据，并转换为map集合
	public static final Map<String, String> ERROR_INFO_MAP = PropertiesUtilss.getMapByProperties("/dict/errorCode/error-info.properties");
}
