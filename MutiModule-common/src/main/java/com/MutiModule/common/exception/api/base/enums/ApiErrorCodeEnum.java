/**
 * File : ApiErrorCodeEnum.java <br/>
 * Author : lenovo <br/>
 * Version : 1.1 <br/>
 * Date : 2017年4月12日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.exception.api.enums <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.exception.api.base.enums;

/**
 * ClassName : ApiErrorCodeEnum <br/>
 * Function : API异常枚举类部分. <br/>
 * Reason : API异常枚举类部分. <br/>
 * Date : 2017年4月12日 下午5:58:17 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

/**
 * 注意， ID 部分已 2的N次方为准 类名：ApiErrorCodeEnum <br />
 *
 * 功能：	异常枚举类
 *
 * @author : alexgaoyh <br />
 * @Date : 2017年4月12日 下午5:59:34 <br />
 * @version : 2017年4月12日 <br />
 */
public enum ApiErrorCodeEnum {

	UNKONWN_EXCEPTION(1l, "未知异常"),
	PARAMS_VALIDATE_FAIL(2L, "参数校验异常"), 
	DATA_DISMATCH(4L, "数据不匹配"),
	FORBIDDEN_DELETE(8L, "禁止删除");

	private Long code;
	private String name;

	private ApiErrorCodeEnum(Long code, String name) {
		this.code = code;
		this.name = name;
	}

	public Long getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
