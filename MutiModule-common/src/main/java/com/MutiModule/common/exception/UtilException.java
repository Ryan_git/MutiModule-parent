package com.MutiModule.common.exception;

import com.MutiModule.common.utils.StringUtilss;

public class UtilException extends RuntimeException{

	public UtilException(Throwable e) {
		super(e.getMessage(), e);
	}
	
	public UtilException(String message) {
		super(message);
	}
	
	public UtilException(String messageTemplate, Object... params) {
		super(StringUtilss.format(messageTemplate, params));
	}
	
	public UtilException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public UtilException(Throwable throwable, String messageTemplate, Object... params) {
		super(StringUtilss.format(messageTemplate, params), throwable);
	}
}
