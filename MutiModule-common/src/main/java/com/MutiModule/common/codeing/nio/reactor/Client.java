/**
 * File : Client.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年7月5日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.codeing.reactor <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.codeing.nio.reactor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
/**
 * ClassName : Client <br/>
 * Function : TODO ADD FUNCTION. <br/>
 * Reason : TODO ADD REASON. <br/>
 * Date : 2017年7月5日 下午1:33:53 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class Client {

	public static void main(String[] args) throws Exception {

		Socket clientSocket = new Socket("localhost", 7070);

		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

		outToServer.writeBytes("This is a message from Client.java File!" + '\n');
		
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "UTF-8"));
		
		// 从服务端返回的值
		String sentence = inFromServer.readLine();
		System.out.println("Response from Server : " + sentence);
		clientSocket.close();

	}
}
