package com.MutiModule.common.fileld.validator.referee;

import java.math.BigDecimal;

import com.MutiModule.common.fileld.validator.AbstractCompareReferee;
import com.MutiModule.common.fileld.validator.Rule.GreaterThan;
import com.MutiModule.common.fileld.validator.State;

/**
 * 对数字进行大于等于比较
 * 
 */
public class GreaterThanReferee extends AbstractCompareReferee<GreaterThan> {

	@Override
	public State check(Object data) {
		// 进行数字转换
		if (!(data instanceof Number)) {
			return failure(getMessageRuleFirst("number.greaterThan", String.format("The field is not type of Number.", fieldName)));
		}
		Object target = getFieldValue(rule.value());
		if (!(target instanceof Number)) {
			return failure(getMessageRuleFirst("number.greaterThan", String.format("The target field is not type of Number.", fieldName)));
		}

		BigDecimal number = new BigDecimal(data + ""), targetNumber = new BigDecimal(target + "");

		if (number.doubleValue() > targetNumber.doubleValue())
			return simpleSuccess();
		else if (rule.equalable() && number.doubleValue() == targetNumber.doubleValue())
			return simpleSuccess();
		else
			return failure(getMessageRuleFirst("number.greaterThan", "The data is not greater than target data"));
	}
	
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
