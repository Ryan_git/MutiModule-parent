package com.MutiModule.common.fileld.validator.referee;

import java.lang.annotation.Annotation;

import org.apache.commons.lang.StringUtils;

import com.MutiModule.common.fileld.validator.AbstractReferee;
import com.MutiModule.common.fileld.validator.Rule.NonNull;
import com.MutiModule.common.fileld.validator.State;

/**
 * 检查数据是否不为空
 * 
 */
public class NonNullReferee extends AbstractReferee<NonNull> {

	@Override
	public State check(Object instance, Object data, Annotation rule, String fieldName) {
		setup(instance, rule, fieldName);
		return data == null ? failure(getMessageRuleFirst("object.nonNull", "The field data is null")) : simpleSuccess();
	}

	@Override
	public State check(Object data) {
		if(data==null) {
			return failure(getMessageRuleFirst("object.nonNull", "The field data is null"));
		}
		return StringUtils.isBlank(data.toString()) ? failure(getMessageRuleFirst("object.nonNull", "The field data is null")) : simpleSuccess();
	}
	
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
