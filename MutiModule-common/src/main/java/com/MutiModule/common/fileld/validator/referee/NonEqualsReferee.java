package com.MutiModule.common.fileld.validator.referee;

import com.MutiModule.common.fileld.validator.AbstractCompareReferee;
import com.MutiModule.common.fileld.validator.Rule.NonEquals;
import com.MutiModule.common.fileld.validator.State;

/**
 * 值不相等比较
 * 
 * data == null && target != null -> success data != null && target == null ->
 * success data equals target -> failure
 * 
 */
public class NonEqualsReferee extends AbstractCompareReferee<NonEquals> {
	@Override
	public State check(Object data) {
		Object target = getFieldValue(rule.value());
		if ((data == null && target != null) || (data != null && target == null))
			return simpleSuccess();
		if (data == null && target == null || data.equals(target))
			return failure(getMessageRuleFirst("object.nonEquals", "The data equals target data."));
		return simpleSuccess();
	}
	
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
