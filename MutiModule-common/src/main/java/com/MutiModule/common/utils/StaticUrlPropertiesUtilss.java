package com.MutiModule.common.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * util类保存了staticUrl单点登陆对应的配置文件的部分参数， 读取的文件为 ： staticUrl.properties 文件，
 * @author alexgaoyh
 *
 */
public class StaticUrlPropertiesUtilss {

	/**
	 * static 静态资源模块对应的url链接部分
	 */
	private static String staticUrl;
	
	static{
		
        loads();
        
    }
	
    synchronized static public void loads(){
    	
        if(StringUtilss.isEmpty(staticUrl)){
            InputStream is = StaticUrlPropertiesUtilss.class.getResourceAsStream("/staticUrl.properties");
            Properties dproperties = new Properties();
            try {
            	dproperties.load(is);
                        
            	staticUrl = dproperties.getProperty("staticUrl").toString();
            	
            }
            catch (Exception e) {
                System.err.println("不能读取属性文件. " + "请确保 staticUrl.properties 在CLASSPATH指定的路径中");
            }
        }
        
    }

	public static String getStaticUrl() {
		return staticUrl;
	}
    
    
}
