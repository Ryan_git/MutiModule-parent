package com.MutiModule.common.json.muti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.jdbc.vo.LocationVO;
import com.MutiModule.common.utils.json.MutiJSONParseUtilss;

public class MulteJSONParseUtilssTest {

	//@Test
	public void testParseJson() {
		String jsonStr = "{\"api\":\"2.1\",\"message\":[\"产品\",\"tokken\"],\"request\":{\"ptype\":\"JK\",\"tokken\":\"A#daDSFkiwi239sdls#dsd\"},\"response\":{\"status\":{\"statusCode\":\"500\",\"statusMessage\":[\"产品类型错误\",\"tokken失效\"]},\"page\":{\"pageSize\":\"100\",\"pageIndex\":\"1\"},\"data\":{\"ptitle\":\"all product lists\",\"sDate\":\"2014-12-01\",\"eDate\":\"2016-12-01\",\"productList\":[{\"pid\":\"RA001\",\"pname\":\"产品1\"},{\"pid\":\"RA002\",\"pname\":\"产品2\"}]}},\"args\":[{\"tit\":\"RA001\",\"val\":\"产品1\"},{\"tit\":\"RA002\",\"val\":\"产品2\"}]}";
		Object obj = MutiJSONParseUtilss.getObjectByJson(jsonStr, "request.tokken", MutiJSONParseUtilss.TypeEnum.string);
		System.out.println(obj.toString());
		
		Object obj2 = MutiJSONParseUtilss.getObjectByJson(jsonStr, "request.aa.tokken", MutiJSONParseUtilss.TypeEnum.string);
		System.out.println(obj2);
		
		// 封装json 
		List<LocationVO> _list = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			LocationVO _vo = new LocationVO();
			_vo.setLat("lat" + i);
			_vo.setLng("lng" + i);
			_list.add(_vo);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", _list);
		String _json = JSONUtilss.toJSon(map);
		
		// 解析json 泛型为list  将list泛型看待成 map
		Object objList = MutiJSONParseUtilss.getObjectByJson(_json, "list", MutiJSONParseUtilss.TypeEnum.arrayList);
		if(objList instanceof ArrayList) {
			List<Map<String, Object>> list = (ArrayList<Map<String, Object>>)objList;
			for (Map<String, Object> map2 : list) {
				System.out.println(map2.get("lat"));
			}
		}
	}
}
