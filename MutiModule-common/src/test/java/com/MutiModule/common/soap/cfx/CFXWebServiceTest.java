package com.MutiModule.common.soap.cfx;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;

public class CFXWebServiceTest {

	// 此例的单元测试部分依赖其他项目，可以直接忽略
	//@Test
	public void test1() {
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://192.168.60.134:9083/helloWorld?wsdl");

		try {
			Object[] objects = client.invoke("say2Params", "alexgaoyh", "content");
			for (int i = 0; i < objects.length; i++) {
				System.out.println(objects[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 外网开放的第三方webservice 接口  传递一个参数
	// @Test
	public void test2() {
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://sso.hner.net/Auth.asmx?wsdl");

		try {
			Object[] objects = client.invoke("GetUserInfo", "UserName");
			for (int i = 0; i < objects.length; i++) {
				System.out.println(objects[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 外网开放的第三方webservice 接口，传递 >1 个参数 
	// @Test
	public void test4() {
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://sso.hner.net/Auth.asmx?wsdl");

		try {
			Object[] objects = client.invoke("GetUserByPermit", "token", "permit");
			for (int i = 0; i < objects.length; i++) {
				System.out.println(objects[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 下面这个单元测试部分失败，原因是此wsdl 文件中存在某种情况 	 undefined element declaration 's:schema'
	// <s:sequence><s:element ref="s:schema"/><s:any/></s:sequence>
	//@Test
	public void test3() {
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://fy.webxml.com.cn/webservices/EnglishChinese.asmx?wsdl");

		try {
			Object[] objects = client.invoke("Translator", "name");
			for (int i = 0; i < objects.length; i++) {
				System.out.println(objects[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}