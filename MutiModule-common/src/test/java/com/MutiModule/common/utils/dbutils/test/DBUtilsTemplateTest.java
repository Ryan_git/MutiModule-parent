package com.MutiModule.common.utils.dbutils.test;

import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.MutiModule.common.utils.dbutils.DBUtilsTemplate;

public class DBUtilsTemplateTest {

	//@Test
	public void testSelect() {
		DBUtilsTemplate templete = new DBUtilsTemplate();
		List<Map<String, Object>> list = templete.find("select * from Demo t where t.createTime < ? and t.createTime > ?", 
				new Object[]{"2015-12-12", "2015-10-10"});
		for(Map<String, Object> map : list) {
			System.out.println(map.get("id"));
		}
	}
	
	/**
	 * 执行如下的sql  语句，返回 name value lng lat 的四种值，此时需要将 lng lat 统一放到一个数组里面，则需要如下的单元测试部分
	 * select name, value, lng, lat from base_location;
	 * 返回值即为可以使用的JavaBean 部分；
	 */
	public void testQuery() {
		String sql = "";
		ResultSetHandler<List<MapMarkPointVO>> listUrlHandler = new BeanListHandler<>(MapMarkPointVO.class, new MapMarkPointHandler());
		DBUtilsTemplate templete = new DBUtilsTemplate();
		List<MapMarkPointVO> list = templete.query(sql, listUrlHandler);
	}
}
