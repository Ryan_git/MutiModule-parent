package com.MutiModule.common.utils.file;

import org.junit.Test;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.utils.JSONUtilss;

public class FileDatabaseHelperTest {

	//@Test
	public void test() {
		FileDatabaseHelper fileDatabaseHelper = new FileDatabaseHelper(null, "fileName.txt");
		
		Long id = IdWorkerInstance.getId();
		
		StudentFileDatabaseEntity student = new StudentFileDatabaseEntity();
		student.setId(id);
		student.setCreateTime(DateUtils.getCurrDateTimeStr());
		student.setModifyTime(DateUtils.getCurrDateTimeStr());
		student.setName("alexgaoyh");
		
		fileDatabaseHelper.addData(JSONUtilss.toJSon(student));
		
		fileDatabaseHelper.printAllData();
		
		student.setName("alexgaoyh11");
		fileDatabaseHelper.updateData(id.toString(), JSONUtilss.toJSon(student));
		
		//fileDatabaseHelper.deleteData(id.toString());
		
		fileDatabaseHelper.printAllData();
		
	}
	
}
