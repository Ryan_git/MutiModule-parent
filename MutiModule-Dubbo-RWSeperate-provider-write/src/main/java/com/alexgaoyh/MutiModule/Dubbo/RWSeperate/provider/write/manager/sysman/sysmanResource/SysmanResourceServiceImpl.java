package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.sysman.sysmanResource;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.write.ISysmanResourceWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResourceMapper;

@Service(value = "sysmanResourceService")
public class SysmanResourceServiceImpl implements ISysmanResourceWriteService{
	
	@Resource(name = "sysmanResourceMapper")
	private SysmanResourceMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysmanResource record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysmanResource record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SysmanResource record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysmanResource record) {
		return mapper.updateByPrimaryKey(record);
	}

}
