package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.dict.dictionary.item;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.item.write.IDictDictionaryItemWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItemMapper;

@Service(value = "dictDictionaryItemService")
public class DictDictionaryItemServiceImpl implements IDictDictionaryItemWriteService{
	
	@Resource(name = "dictDictionaryItemMapper")
	private DictDictionaryItemMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(DictDictionaryItem record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(DictDictionaryItem record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(DictDictionaryItem record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DictDictionaryItem record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int deleteByDictid(String dictId) {
		return mapper.deleteByDictid(dictId);
	}
	
}
