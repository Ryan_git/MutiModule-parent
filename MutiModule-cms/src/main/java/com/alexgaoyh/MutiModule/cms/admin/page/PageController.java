package com.alexgaoyh.MutiModule.cms.admin.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 頁面管理
 * @author alexgaoyh
 *
 */
@Controller
@RequestMapping(value="/page/page")
public class PageController {

	/**
	 * 頁面管理 - 首頁
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public ModelAndView list(ModelAndView model) {
		
		model.setViewName("/admin/page/page/index");
		return model;
	}
}
