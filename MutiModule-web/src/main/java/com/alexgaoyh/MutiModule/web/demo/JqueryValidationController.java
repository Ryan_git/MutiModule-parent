package com.alexgaoyh.MutiModule.web.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="jqueryValidation")
public class JqueryValidationController {

	
	@RequestMapping(value = "register", method = RequestMethod.GET)
	public ModelAndView register(ModelAndView model) {
		
		model.setViewName("jqueryValidation/register");
		
		return model;
	}
}
